//
//  MainViewController.m
//  VCTransitions
//
//  Created by Tyler Tillage on 7/3/13.
//  Copyright (c) 2013 CapTech. All rights reserved.
//

#import "MainViewController.h"
#import "ImageViewController.h"
#import "ScaleAnimation.h"

@interface MainViewController () {
    ScaleAnimation *_scaleAnimationController;
}
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 200, 100)];
    [btn setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:btn];
    
    [btn addTarget:self action:@selector(btnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    //Load our animation controllers
    _scaleAnimationController = [[ScaleAnimation alloc] initWithNavigationController:self.navigationController];
}


-(void) btnOnClick:(UIButton*)sender{
    ImageViewController *page = [[ImageViewController alloc] init];
    page.transitioningDelegate = self;//指定转场动画的代理;
    page.modalPresentationStyle = UIModalPresentationCustom;
    _scaleAnimationController.viewForInteraction = page.view;
    [self.navigationController pushViewController:page animated:YES];
    //[self presentViewController:page animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _scaleAnimationController.viewForInteraction = nil;
}


-(IBAction)showOptions:(id)sender {
}

/**
 *  MARK:--------------------UIViewControllerTransitioningDelegate--------------------
 */
#pragma mark - Transitioning Delegate (Modal)
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    _scaleAnimationController.type = AnimationTypePresent;
    return _scaleAnimationController;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    _scaleAnimationController.type = AnimationTypeDismiss;
    return _scaleAnimationController;
}

/**
 *  MARK:--------------------UINavigationControllerDelegate--------------------
 */
#pragma mark - Navigation Controller Delegate
-(id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    BaseAnimation *animationController = _scaleAnimationController;
    switch (operation) {
        case UINavigationControllerOperationPush:
            animationController.type = AnimationTypePresent;
            return  animationController;
        case UINavigationControllerOperationPop:
            animationController.type = AnimationTypeDismiss;
            return animationController;
        default: return nil;
    }
}

-(id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    if ([animationController isKindOfClass:[ScaleAnimation class]]) {
        ScaleAnimation *controller = (ScaleAnimation *)animationController;
        if (controller.isInteractive) return controller;
        else return nil;
    } else return nil;
}

@end
